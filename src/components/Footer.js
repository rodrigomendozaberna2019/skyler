import React from 'react';
import ContactForm from './ContactForm';
import config from '../../config';
export default function Footer() {
  return (
    <footer id="footer">
      <div className="inner">
        <section>
          <h2>Información</h2>
        </section>
        <section>
          <h2>Redes sociales</h2>
          <ul className="icons">
            {config.socialLinks.map(social => {
              const { icon, name, url } = social;
              return (
                <li key={url}>
                  <a href={url} className={`icon ${icon}`}>
                    <span className="label">{name}</span>
                  </a>
                </li>
              );
            })}
          </ul>
        </section>
        <section>
          <h2>Ayuda</h2>
        </section>
        <ul className="copyright">
          <li>&copy; 2020 Skyler Finance</li>
        </ul>
      </div>
    </footer>
  );
}
