import React from "react";
import { Link } from "gatsby";

export default function SubNav({ onClose = () => {} }) {
  return (
    <nav id="submenu">
      <div className="inner">
        <h2>Menu</h2>
        <ul>
          <li>
            <Link to="/">Producto 1</Link>
          </li>
          <li>
            <Link to="/">Producto 2</Link>
          </li>
          <li>
            <Link to="/">Producto 3</Link>
          </li>
        </ul>
      </div>
      <a
        className="close"
        onClick={(e) => {
          e.preventDefault();
          onClose();
        }}
        href="#menu"
      >
        Close
      </a>
    </nav>
  );
}
