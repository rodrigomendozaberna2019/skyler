import React from "react";

import Layout from "../components/Layout";

const IndexPage = () => (
  <Layout>
    <div id="main">
      <div className="inner">
        <h1 style={{ color: "#000000" }}>Home</h1>

        <div>
          
        </div>
      </div>
    </div>
  </Layout>
);

export default IndexPage;
