import React from "react";

import Layout from "../components/Layout";

const IndexPage = () => (
  <Layout>
    <div id="main">
      <div className="inner">
        <h1 style={{ color: "#000000" }}>Servicios</h1>
      </div>
    </div>
  </Layout>
);

export default IndexPage;
 